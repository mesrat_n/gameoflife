module.exports = {
    UNHANDLED_ERROR: {
        code: -1,
        error: 'Unknown error',
        httpCode: 520
    },
    FILE_ERROR: {
        code: 10,
        error: 'File error or bad file format.',
        httpCode: 400
    },
}

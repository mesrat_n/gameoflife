#!/bin/bash

docker build -t gameoflife:v2 ./API
docker build -t gameoflife-web:v2 ./APP

docker tag gameoflife:v2 gcr.io/gameoflife-252015/gameoflife:v2
docker push gcr.io/gameoflife-252015/gameoflife:v2

docker tag gameoflife-web:v2 gcr.io/gameoflife-252015/gameoflife-web:v2
docker push gcr.io/gameoflife-252015/gameoflife-web:v2

kubectl apply -f ./Kube

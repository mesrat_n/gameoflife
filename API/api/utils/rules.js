class Rules {
    constructor(map) {
        let self = this

        this.map = map
        this.rowLimit = this.map.length-1;
        this.columnLimit = this.map[0].length-1;
    }

    getAliveSurroundings(i, j) {
        let alive_surrounding = 0
        for(var x = Math.max(0, i-1); x <= Math.min(i+1, this.rowLimit); x++) {
            for(var y = Math.max(0, j-1); y <= Math.min(j+1, this.columnLimit); y++) {
                if(x !== i || y !== j) {
                    if (true == this.map[x][y])
                        alive_surrounding++
                }
            }
        }
        return alive_surrounding
    }

    next_state(state, alive_surrounding) {
        return state? (2 <= alive_surrounding && 3 >= alive_surrounding) : (3 == alive_surrounding)
    }
}

module.exports = Rules




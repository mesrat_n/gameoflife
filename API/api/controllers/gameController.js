const path   = require('path');
const fs          = require('fs')
const Parser = require("../utils/parsing");
const Game   = require("../utils/game");
const errorsConstants = require('../constants/errors');

exports.postCycles = async (req, res, next) => {
    try {
        if (Object.keys(req.files).length == 0) {
            throw errorsConstants.FILE_ERROR
        }

        let mapFile = req.files.mapFile;
        let my_path = path.join(__dirname, '/../files/'+ mapFile.name);

        mapFile.mv(my_path, function(err) {
            if (err) throw err
            let parsing = new Parser(my_path)
            let map = [];
            map = parsing.getContent()
            let dimensions = parsing.getDimensions()

            let maxCycle = req.body.maxCycle? req.body.maxCycle: 5
            let game = new Game(maxCycle, map, dimensions)

            let all_cycles = game.game()
            parsing.createFile(all_cycles)

            rs = fs.createReadStream(__dirname + '/../files/all_cycles.txt');
            res.attachment("all_cycles.txt"); 
            rs.pipe(res);
        });
    } catch (e) {
        next(e);
    }
}

exports.postCyclesArray = async (req, res, next) => {
    try {
        let maxCycle   = req.body.maxCycle? req.body.maxCycle: 5
        let map        = req.body.map
        let dimensions = [map.length, map[0].length]
        
        let game = new Game(maxCycle, map, dimensions)
        let all_cycles = game.game()
        res.status(200).send(all_cycles);
    } catch (e) {
        next(e);
    }
}

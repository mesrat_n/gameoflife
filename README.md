# GameOfLife

This is a Kata for Wemanity
A game of life is a cellular automaton devised by the British mathematician John Horton Conway.

This Kata consists in an API written in NodeJS that calculates the evolution in every generation and a web app for the user to design his grid and see the evolution.

Here is the kata http://codingdojo.org/kata/GameOfLife/

I will be using TDD, docker and kubernetes. For the CICD pipelines I will be using the ones provided by Gitlab.

## What I did

- API : 
    - Method : TDD, Issues. First time using this ! All the tests were not written before the functions but I tried my best

- Front :
    - I did not put much effort here, I wanted to concentrate mostly in the CICD aspect

- CICD : 
    - For the merge request, it runs the tests (unit and functional)
    - Only for master : 
        Using the GitLab Runner in the Kubernetes Cluster
        Build the docker
        Push the image to Google Cloud Registry
        Create the Pod
        Create the service
    I used Kubernetes and Google Cloud for the first time in this project. It was challenging, a lot to learn but I managed to have something useful.

- Monitoring : 
    - Prometheus showing the Cluster health

## What can be done

- A nicer front to see the evolution of the cycles
- More tests
- A customized monitoring
- An auth for the API

## Troubleshooting

- The API URL in the front is not changed everytime with the new deployment
- Cannot config a static IP address

## TRY IT !

API : http://35.195.136.220:3000/
WEB : http://35.187.31.234/

Map exemple : 

```
Generation 1:
4 5
.....
.**..
..*..
..*..
```

*To run the API you need at least node:11.10*

Commands : 

```
npm install
npm test
npm start
```

FOR THE CLI : 

`node API/api/utils/test_utils.js`



//Require the dev-dependencies
const chai = require('chai'),
    chaiHttp = require('chai-http'),
    server = require('../server'),
    should = chai.should();

// Use http requests
chai.use(chaiHttp);

// Required testers
const expect = chai.expect;
const path = require('path');

const map_1  = path.join(__dirname, '/map_1.txt');

const bool_map  = [
    [false, false, false, false, false],
    [false, true, true, false, false],
    [false, false, true, false, false],
    [false, false, true, false, false]
];

describe('Game of Life', function () {
    this.timeout(30000);

    beforeEach((done) => {
        //Before each test if needed
        done();
    });

    describe('/GET API', () => {
        it('it should state the status of the API', (done) => {
            chai.request(server)
                .get('/')
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(200);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('string');
                    
                    // Check heros reponse values
                    expect(res.body).to.be.equal("Game Of Life API is working");

                    done();
                });
        });
    });

    describe('/POST Game', () => {
        it('it should detect that there is no file', (done) => {
            let file = {
                fileName: "map_1.tx",
            }
            chai.request(server)
                .post('/game')
                .send(file)
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(520);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('object');
                    res.body.error.should.be.a('string');
                    
                    // Check heros reponse values
                    expect(res.body.error).to.be.equal("TypeError: Cannot convert undefined or null to object");

                    done();
                });
        });
    });

    describe('/POST Game', () => {
        it('it should detect the file and send back a file', (done) => {
            chai.request(server)
                .post('/game')
                .field('Content-Type', 'multipart/form-data')
                .field('fileName', 'map_1.txt')
                .attach('files', map_1)
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(520);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('object');
                    res.body.error.should.be.a('string');
                    
                    // Check heros reponse values
                    expect(res.body.error).to.be.equal("TypeError: Cannot read property 'name' of undefined");

                    done();
                });
        });
    });

    describe('/POST Game with array', () => {
        it('it should detect the array and send back the complete aray', (done) => {
            chai.request(server)
                .post('/game/array')
                .send({
                    maxCycle: 5,
                    map: bool_map
                })
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(200);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('array');
                    
                    // Check heros reponse values
                    expect(res.body.length).to.be.equal(5);

                    var result = [ { generation: 1,
                        dimensions: '4 5',
                        map: [  [ false, false, false, false, false ],
                        [ false, true, true, false, false ],
                        [ false, false, true, false, false ],
                        [ false, false, true, false, false ] ] },
                      { generation: 2,
                        dimensions: '4 5',
                        map: [  [ false, false, false, false, false ],
                        [ false, true, true, false, false ],
                        [ false, false, true, true, false ],
                        [ false, false, false, false, false ] ] },
                      { generation: 3,
                        dimensions: '4 5',
                        map: [  [ false, false, false, false, false ],
                        [ false, true, true, true, false ],
                        [ false, true, true, true, false ],
                        [ false, false, false, false, false ] ] },
                      { generation: 4,
                        dimensions: '4 5',
                        map: [ [ false, false, true, false, false ],
                        [ false, true, false, true, false ],
                        [ false, true, false, true, false ],
                        [ false, false, true, false, false ] ] },
                      { generation: 5,
                        dimensions: '4 5',
                        map: [ [ false, false, true, false, false ],
                        [ false, true, false, true, false ],
                        [ false, true, false, true, false ],
                        [ false, false, true, false, false ] ] } ];
                    
                    expect(res.body).to.eql(result);

                    done();
                });
        });
    });
});

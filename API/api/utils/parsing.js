const lineByLine  = require('n-readlines')
const path        = require('path')
const fs          = require('fs')

const default_map = path.join(__dirname, '/map_1.txt')



class Parser {
    constructor(filename = default_map) {
        let self = this

        this.filename = filename
        this.hight    = 0
        this.width    = 0
    }

    getDimensions() {
        return[this.hight, this.width]
    }

    setDimensions(hight, width) {
        this.hight = hight
        this.width = width
    }

    createFile(all_cycles) {
        var all_content = ''

        all_cycles.forEach(cycle => {
            all_content += 'Generation : ' + cycle.generation + '\n'
            all_content += cycle.dimensions + '\n'
            for (let i = 0; i < cycle.dimensions[0]; i++) {
                let line = ''
                for (let j = 0; j < cycle.dimensions[2]; j++) {
                    if (true == cycle.map[i][j])
                        line += ('*')
                    else
                        line += ('.')
                }
                all_content += line + '\n'
            }
            all_content += '\n'
        });

        var final_map = path.join(__dirname, '/../files/all_cycles.txt')
        var stream = fs.createWriteStream(final_map);
        stream.once('open', function(fd) {
            stream.write(all_content);
            stream.end();
        });
    }

    getContent() {
        let i = 0;
        let map = []
        if (null == this.filename)
            return false

        const liner = new lineByLine(this.filename)
        let line;
 
        while (line = liner.next()) {
            if (i >= 2) {
                if (line.length < 3)
                    return ('Error : Small map width')
                var map_line = []
                for(var j = 0; j < line.length; j++) {
                    if (line[j] == 42)
                        map_line.push(true)
                    else if (line[j] == 46)
                        map_line.push(false)
                    else
                        return('Error : Cannot parse map')
                }
                map.push(map_line)
            }
            i++;
        }
        this.setDimensions(i-2, j)
        if (i < 5)
            return ('Error : Small map hight')
        return map
    }
}


module.exports = Parser

const chai = require('chai'),
    should = chai.should(),
    expect = chai.expect;

const path = require('path');

const map_1           = path.join(__dirname, '/map_1.txt');
const small_map_width = path.join(__dirname, '/small_map_width.txt');
const small_map_hight = path.join(__dirname, '/small_map_hight.txt');
const wrong_map       = path.join(__dirname, '/wrong.txt');

const bool_map  = [
    [false, false, false, false, false],
    [false, true, true, false, false],
    [false, false, true, false, false],
    [false, false, true, false, false]
];


const Parser = require("../api/utils/parsing");
const Rules  = require("../api/utils/rules");
const Game   = require("../api/utils/game");

const rules = new Rules(bool_map);


describe('Read map test', () => {

    beforeEach(() => {
        //Before each test if needed
    });

    describe('#readFile method' , () => {

        it('when i get a txt file, I can transform it to an array', () => {
            var parsing = new Parser(map_1);
            var template = [];
            template = parsing.getContent();

            var result = [
                [false, false, false, false, false],
                [false, true, true, false, false],
                [false, false, true, false, false],
                [false, false, true, false, false]
            ];
            template.should.be.a('array');
            expect(template.length).to.be.equal(4);
            expect(result).to.eql(template);
        });

        //No filename given
        it('when i dont pick a filename, I use the default map', () => {
            var parsing = new Parser();
            var template = [];
            template = parsing.getContent();

            var result = [
                [false, false, false, false, false],
                [false, true, true, false, false],
                [false, false, true, false, false],
                [false, false, true, false, false]
            ];
            template.should.be.a('array');
            expect(template.length).to.be.equal(4);
            expect(result).to.eql(template);
        });

        //minimum 4x4
        it('when i get a map width lesser than 3, I send back an error', () => {
            var parsing = new Parser(small_map_width);
            var template = '';
            template = parsing.getContent();
            
            template.should.be.a('string');
            expect(template).to.eql('Error : Small map width');
        });

        it('when i get a map hight lesser than 3, I send back an error', () => {
            var parsing = new Parser(small_map_hight);
            var template = '';
            template = parsing.getContent();
            
            template.should.be.a('string');
            expect(template).to.eql('Error : Small map hight');
        });

        //not . nor * 
        it('when i get a wrong map, I send back an error', () => {
            var parsing = new Parser(wrong_map);
            var template = [];
            template = parsing.getContent();
            
            template.should.be.a('string');
            expect(template).to.eql('Error : Cannot parse map');
        });

    });

    describe('#getAliveSurrounding method', () => {
        //Edge cell
        it('when i give an edge cell, I can have the surroundings', () => {
            var alive_surrounding = 0;
            alive_surrounding = rules.getAliveSurroundings(0, 0);

            alive_surrounding.should.be.a('number');
            expect(alive_surrounding).to.eql(1);
        });

        //Middle cell
        it('when i give a middle cell, I can have the surroundings', () => {
            var alive_surrounding = 0;
            alive_surrounding = rules.getAliveSurroundings(1, 2);

            alive_surrounding.should.be.a('number');
            expect(alive_surrounding).to.eql(2);
        });
    })

    describe('#getNextState method', () => {
        //alive to alive
        it('when i give a living cell with 2 alive neighbours, the cell lives', () => {
            next_gen = rules.next_state(true, 2);

            next_gen.should.be.a('boolean');
            expect(next_gen).to.eql(true);
        });

        it('when i give a living cell with 3 alive neighbours, the cell lives', () => {
            next_gen = rules.next_state(true, 3);

            next_gen.should.be.a('boolean');
            expect(next_gen).to.eql(true);
        });

        //alive to dead
        it('when i give a living cell with less than 2 alive neighbours, the cell dies', () => {
            next_gen = rules.next_state(true, 1);

            next_gen.should.be.a('boolean');
            expect(next_gen).to.eql(false);
        });

        it('when i give a living cell with more than 3 alive neighbours, the cell dies', () => {
            next_gen = rules.next_state(true, 4);

            next_gen.should.be.a('boolean');
            expect(next_gen).to.eql(false);
        });

        //dead to alive
        it('when i give a dead cell with 3 alive neighbours, the cell lives', () => {
            next_gen = rules.next_state(false, 3);

            next_gen.should.be.a('boolean');
            expect(next_gen).to.eql(true);
        });

        //dead to dead
        it('when i give a dead cell with less or more than 3 alive neighbours, the cell stays dead', () => {
            next_gen = rules.next_state(false, 2);

            next_gen.should.be.a('boolean');
            expect(next_gen).to.eql(false);
        });
    })

    describe('#nextGen method' , () => {
        it('when i get a map whit no future, I know that it wont change', () => {
            var map = [
                [false, false, false, false, false],
                [false, false, false, false, false],
                [false, false, false, false, false],
                [false, false, false, false, false]
            ];
            
            var game = new Game(5, map);
            var next_gen = game.nextCycle(map);
            var result = [
                [false, false, false, false, false],
                [false, false, false, false, false],
                [false, false, false, false, false],
                [false, false, false, false, false]
            ];
            
            next_gen.should.be.a('array');
            expect(next_gen.length).to.be.equal(4);
            expect(next_gen).to.eql(result);
        });

        it('when i get a map, I know how the next gen will be', () => {
            var map = [
                [false, false, false, false, false],
                [false, true, true, false, false],
                [false, false, true, false, false],
                [false, false, true, false, false]
            ];
            
            var game = new Game(5, map);
            var next_gen = game.nextCycle(map);
            var result = [
                [false, false, false, false, false],
                [false, true, true, false, false],
                [false, false, true, true, false],
                [false, false, false, false, false]
            ];
            
            next_gen.should.be.a('array');
            expect(next_gen.length).to.be.equal(4);
            expect(next_gen).to.eql(result);
        });
    });

    describe('#Game method' , () => {
        it('With the number of generations, the map and the dimension, I can have all the cycles', () => {
            var map = [
                [false, false, false, false, false],
                [false, true, true, false, false],
                [false, false, true, false, false],
                [false, false, true, false, false]
            ];
            
            var game = new Game(5, map);
            var all_cycles = game.game();
            
            var result = [ { generation: 1,
                dimensions: '4 5',
                map: [  [ false, false, false, false, false ],
                [ false, true, true, false, false ],
                [ false, false, true, false, false ],
                [ false, false, true, false, false ] ] },
              { generation: 2,
                dimensions: '4 5',
                map: [  [ false, false, false, false, false ],
                [ false, true, true, false, false ],
                [ false, false, true, true, false ],
                [ false, false, false, false, false ] ] },
              { generation: 3,
                dimensions: '4 5',
                map: [  [ false, false, false, false, false ],
                [ false, true, true, true, false ],
                [ false, true, true, true, false ],
                [ false, false, false, false, false ] ] },
              { generation: 4,
                dimensions: '4 5',
                map: [ [ false, false, true, false, false ],
                [ false, true, false, true, false ],
                [ false, true, false, true, false ],
                [ false, false, true, false, false ] ] },
              { generation: 5,
                dimensions: '4 5',
                map: [ [ false, false, true, false, false ],
                [ false, true, false, true, false ],
                [ false, true, false, true, false ],
                [ false, false, true, false, false ] ] } ];
            
            all_cycles.should.be.a('array');
            expect(all_cycles.length).to.be.equal(5);
            expect(all_cycles).to.eql(result);
        });

        
    });
});

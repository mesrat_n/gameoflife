'use strict';

const defaultController = require('./controllers/defaultController');
const gameController    = require('./controllers/gameController');

module.exports = function (app) {

	/**
	 * Basic route
	 */
    app.route('/')
        .get(defaultController.getResponse);

    app.route('/game')
        .post(gameController.postCycles)
    
    app.route('/game/array')
        .post(gameController.postCyclesArray)

    app.use((err, req, res, next) => {
        if (err) {
            if (err.httpCode)
                res.status(err.httpCode).send({ error: err.error });
            else
                res.status(520).send({ error: err + "" });
        } else {
            res.status(404).send({ error: req.originalUrl + ' not found' });
        }
    });
}

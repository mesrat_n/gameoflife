const express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    morgan = require('morgan'),
    bodyParser = require('body-parser')
    fileUpload = require('express-fileupload');;

// Config API
if (process.env.NODE_ENV !== 'test')
    app.use(morgan('combined'))

app.use(bodyParser.json({
    limit: '50mb',
    extended: true
}));

app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));

app.use(fileUpload()); 

// Load routes & 404
//importing routes
var routes = require('./api/router');
//register the route
routes(app);

// Start API
app.listen(port, () => {
    console.log(`Game Of Life RESTful API server started on ${port}`)
  })


// Export app for testings
module.exports = app;


const Parser = require("./parsing");
const Game   = require("./game");
var fs       = require('fs');

var parsing = new Parser("./map_1.txt");

var map = [];
// console.log(map);
map = parsing.getContent();
var dimensions = parsing.getDimensions();

//game
var game = new Game(5, map, dimensions);

var all_cycles = game.game();
//console.log(all_cycles)
var all_content = ''

all_cycles.forEach(cycle => {
    all_content += 'Generation : ' + cycle.generation + '\n'
    all_content += cycle.dimensions + '\n'
    for (let i = 0; i < cycle.dimensions[0]; i++) {
        let line = ''
        for (let j = 0; j < cycle.dimensions[2]; j++) {
            if (true == cycle.map[i][j])
                line += ('*')
            else
                line += ('.')
        }
        all_content += line + '\n'
    }
    all_content += '\n'
});

console.log(all_content)

var stream = fs.createWriteStream("./all_cycles.txt");
stream.once('open', function(fd) {
  stream.write(all_content);
  stream.end();
});



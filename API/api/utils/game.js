const Rules = require("./rules");

class Game {
    constructor(max_cycle, initial_map, dimensions = [4, 5]) {
        let self = this

        this.max_cycle = max_cycle
        this.initial_map = initial_map
        this.dimensions = dimensions[0] + " " + dimensions[1]

        this.all_cycles = [
            {
                "generation": 1,
                "dimensions": this.dimensions,
                "map": initial_map
            }
        ]
    }

    nextCycle(map) {
        var state = true
        var alive_surrounding = 0
        var next_state = true
        var rules = new Rules(map);
        var next_gen = JSON.parse(JSON.stringify(map))
        for (let i = 0; i < map.length; i++) {
            for (let j = 0; j < map[0].length; j++) {
                state = map[i][j]
                alive_surrounding = rules.getAliveSurroundings(i, j)
                next_state = rules.next_state(state, alive_surrounding)
                if (next_state != state)
                    next_gen[i][j] = next_state
            }
        }
        return next_gen
    }

    game() {
        var cycle = 2
        var next_map = []
        while (cycle <= this.max_cycle) {
            var next_cycle = {
                "generation": 0,
                "dimensions": this.dimensions,
                "map": []
            }
            if (0 == next_map.length)
                next_map = this.nextCycle(this.initial_map)
            else
                next_map = this.nextCycle(next_map)
            next_cycle.generation = cycle
            next_cycle.map = next_map
            this.all_cycles.push(next_cycle)
            cycle++
        }
        return this.all_cycles
    }
}

module.exports = Game
